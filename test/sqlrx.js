"use strict";
import assert from "assert";
import SqlRx from "../src/sqlrx";
import TestDb from "./testdb";

let db = new SqlRx.Db(TestDb.createDb());

describe("SqlRx", () => {
  describe("Query", () => {
    it("works and stuff", (done) => {
      let employees = db.query("select * from employees")
        .asRows(TestDb.employee)
        .first()
        .subscribe((employee) => {
          assert.strictEqual(employee.username, "alice");
          done();
        });
    });

    it("maps columns correctly", (done) => {
      let employees = db.query("select name from employees")
        .asRows(TestDb.employee)
        .first()
        .subscribe((employee) => {
          assert.strictEqual(employee.name, "Alice Allison");
          done();
        });
    });
  });
});

