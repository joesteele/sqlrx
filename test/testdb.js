"use strict";
import SQL from "sql.js";

const EMPLOYEE_TABLE = "employees";

let employee = {
  id: "_id", username: "username", name: "name"
};

const CREATE_EMPLOYEE = `CREATE TABLE ${EMPLOYEE_TABLE} (`
  + `${employee.id} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `
  + `${employee.username} TEXT NOT NULL UNIQUE, `
  + `${employee.name} TEXT NOT NULL)`

function insertEmployee(username, name) {
  return `INSERT INTO ${EMPLOYEE_TABLE} (${employee.username}, ${employee.name}) `
    + `VALUES ("${username}", "${name}")`
}

function createDb() {
  let db = new SQL.Database()

  db.exec(CREATE_EMPLOYEE);
  db.exec(insertEmployee("alice", "Alice Allison"));
  db.exec(insertEmployee("bob", "Bob Bobberson"));
  db.exec(insertEmployee("eve", "Eve Evenson"));

  return db;
}

let TestDb = {
  employee,
  createDb
}

export default TestDb;

