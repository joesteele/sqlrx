"use strict";
import Rx from "rx";
import SQL from "sql.js";

let mapper = (projection, columns, row) => {
  let result = {};
  for (var i=0; i < columns.length; i++) {
    let column = columns[i];
    let value = row[i];
    let property = projection[column];
    result[property] = value;
  }
  return result;
};

class Query {
  constructor(db, sql) {
    this.db = db;
    this.sql = sql;
  }

  run() {
    return this.db.exec(this.sql);
  }

  asRows(columns) {
    return Rx.Observable.create((observer) => {
      let results = this.run();

      let result = results[0];
      if (typeof result === "undefined") {
        observer.onNext([]);
      } else {
        let projection = {};
        for (let key of Object.keys(columns)) {
          projection[columns[key]] = key;
        };

        for (let row of result.values) {
          observer.onNext(mapper(projection, result.columns, row));
        }
      }

      observer.onCompleted();
    });
  }
}

class Db {
  constructor(db) {
    if (typeof db !== "undefined") {
      this.db = db;
    } else {
      this.db = new SQL.Database();
    }
  }

  query(sql) {
    return new Query(this.db, sql);
  }

  exec(sql) {
    return this.db.exec(sql);
  }
}

let SqlRx = {
  Query,
  Db
};

export default SqlRx;

